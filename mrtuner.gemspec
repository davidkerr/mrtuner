Gem::Specification.new do |s|
  s.name        = 'mrtuner'
  s.version     = '0.6.1'
  s.date        = '2016-06-05'
  s.summary     = "Tunes PostgreSQL Instances"
  s.description = "Generates a useful base configuration for PostgreSQL"
  s.authors     = ["David Kerr"]
  s.email       = 'dave@davidmerr.com'
  s.files       = [ "lib/mrtuner.rb",
                    "lib/util.rb",
                    "bin/mrtuner",
                    "license.txt",
                    "contributors.txt"]
  s.homepage    = 'https://bitbucket.org/davidkerr/mrtuner'
  s.license     = 'BSD'
  s.add_runtime_dependency 'trollop','~>2.0'
  s.add_runtime_dependency 'filesize', '~> 0'
  s.bindir      = 'bin'
  s.executables << 'mrtuner'
end

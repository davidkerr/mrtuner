class MrTuner

  require 'util.rb'

  def initialize(type, memory, config = {})
    @type                      = type.downcase
    @memory                    = memory * 1073741824
    @connections               = config['connections']
    @oversubscribe             = config.key?("oversubscribe") ? config['oversubscribe'] : config['connections']
    @block_size                = config.key?("block_size") ? config['block_size'] : 8192
    @wal_block_size            = config.key?("wal_block_size") ? config['block_size'] : 8192
    @max_locks_per_transaction = config.key?("max_locks_per_transaction") ? config['max_locks_per_transaction'] : 64
    @autovacuum_max_workers    = config.key?("autovacuum_max_workers") ? config['autovacuum_max_workers'] : 3
    @max_prepared_transactions = config.key?("max_prepared_transactions") ? config['max_prepared_transactions'] : 0
    @page_size                 = config.key?("page_size") ? config['page_size'] : 4096
  end

  def get_config
    {
      :max_connections              => max_connections,
      :maintenance_work_mem         => make_round_number(maintenance_work_mem),
      :work_mem                     => make_round_number(work_mem.round),
      :checkpoint_completion_target => checkpoint_completion_target,
      :shared_buffers               => make_round_number(shared_buffers),
      :effective_cache_size         => make_round_number(effective_cache_size),
    }
  end

  def display_config
    get_config.each do |k,v|
      case k
      when :maintenance_work_mem,:work_mem,:shared_buffers,:effective_cache_size
        puts "#{k} = #{as_size(v)}"
      else
        puts "#{k} = #{v}"
      end
    end
  end

  private
  def max_connections
    return @oversubscribe if @oversubscribe
    case @type
    when 'oltp'
      @connections = 300
    when 'dw'
      @connections = 20
    when 'mixed'
      @connections = 80
    when 'desktop'
      @connections = 5
    else
      @connections = 200
    end
  end

  def shared_buffers
    case @type
    when 'oltp','dw','web','mixed'
      @memory / 4
    else
      @memory / 16
    end
  end

  def effective_cache_size
    # return 134217728 if @memory <  268435456
    # return 2147483648 if @memory < 2147483648
    case @type
    when 'oltp','dw','web','mixed'
      @memory * 0.75
    else
      @memory / 4
    end
  end

  def work_mem
    case @type
    when 'web', 'oltp'
      @memory / @connections
    when 'dw', 'mixed'
      @memory / @connections / 2
    when 'desktop'
      @memory / @connections / 6
    end
  end

  def maintenance_work_mem
    case @type
    when 'web', 'oltp', 'mixed', 'desktop'
      mwm = @memory / 16
    when 'dw'
      mwm = @memory / 8
    end
    mwm > 1073741824 ? 1073741824 : mwm
  end

  def checkpoint_completion_target
    case @type
    when 'web'
      0.7
    when 'oltp', 'dw', 'mixed'
      0.9
    when 'desktop'
      0.5
    end
  end


end

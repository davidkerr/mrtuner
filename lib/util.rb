require 'filesize'

def as_size( s )
  prefix = %W(TB GB MB Kb B).freeze

  s = s.to_f
  i = prefix.length - 1
  while s > 512 && i > 0
    i -= 1
    s /= 1024
  end
  ((s > 9 || s.modulo(1) < 0.1 ? '%d' : '%.1f') % s).to_f.round.to_s +
    (prefix[i] == 'B' ? '' : prefix[i] )
end

def make_round_number( s )
  #
  # So yeah, this sucks. but PG uses 'GB' nomenclature meaning 'GiB'
  #
  s = as_size(s)
  s.gsub!('TB','TiB')
  s.gsub!('GB','GiB')
  s.gsub!('MB','MiB')
  s.gsub!('KB','KiB')
  Filesize.from(s).to_f('B')
end
